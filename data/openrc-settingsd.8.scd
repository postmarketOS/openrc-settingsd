OPENRC-SETTINGSD(8)

# NAME

openrc-settingsd - system settings D-Bus service for OpenRC

# SYNOPSIS

*openrc-settingsd* [*--debug*] [*--syslog*] [*--read-only*] [*--ntp-service*=_SERVICE_] [*--update-rc-status*]

# DESCRIPTION

The *openrc-settingsd* daemon implements the standard hostnamed (i.e.
_org.freedesktop.hostname1_), localed (i.e. _org.freedesktop.locale1_), and
timedated (_org.freedesktop.timedate1_) D-Bus interfaces for OpenRC systems.
Users and administrators should not need to launch the *openrc-settingsd*
executable manually. Depending on the installation, it will either be launched
automatically via D-Bus activation when needed, or started by the administrator
as an OpenRC service using _/etc/init.d/openrc-settingsd_.

# OPTIONS

*--help*
	Show help options.

*--version*
	Show version information.

*--debug*
	Enable debugging messages.

*--syslog*
	Log to syslog.

*--read-only*
	Run daemon in read-only mode. It will read settings files, but will not
	modify them.

*--ntp-service*=_SERVICE_
	Specify which OpenRC service to use as the NTP service. If this option is
	not used, *openrc-settingsd* will attempt to autodetect an appropriate NTP
	implementation.

*--update-rc-status*
	Automatically set the status of the _openrc-settingsd_ service to _started_
	when the daemon successfully starts, and to _stopped_ when the daemon stops.
	If *openrc-settingsd* is manually launched with this argument, the
	administrator will be able to stop it via _/etc/init.d/openrc-settingsd
	stop_.

# AUTHORS

Written by Alexandre Rostovtsev ⟨tetromino@gentoo.org⟩.

# BUGS

Bug reports should be submitted to the issue tracker
⟨https://gitlab.com/postmarketOS/openrc-settingsd/-/issues⟩.

# SEE ALSO

*dbus-daemon*(1), *polkit*(8), *rc*(8)
